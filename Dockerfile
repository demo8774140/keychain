FROM golang:latest

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY . .

# RUN go build -o pass-shieldy

# CMD ["./pass-shieldy"]
CMD ["go", "run", "main.go"]
