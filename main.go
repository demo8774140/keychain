package main

import (
    "gitlab.com/bitrewery/pass-shieldy/app/routes"

    "github.com/gin-gonic/gin"
)

func main() {
    router := gin.Default()

    routes.Load(router)

    router.Run(":8080")
}