package config

type CryptoConfig struct {
    SecretLength int
}

func LoadCryptoConfig() *CryptoConfig {
    return &CryptoConfig{
        SecretLength: 32,
    }
}
