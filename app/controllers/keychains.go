package controllers

import (
    "gitlab.com/bitrewery/pass-shieldy/app/config"
    "gitlab.com/bitrewery/pass-shieldy/app/domain"
    "gitlab.com/bitrewery/pass-shieldy/app/services"

    "encoding/json"
    "github.com/gin-gonic/gin"
    "math/rand"
    "net/http"
    "time"
)

func EncryptKeychain(context *gin.Context) {
    var keychain domain.Keychain
    if err := context.BindJSON(&keychain); err != nil {
        context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    keychainBytes, err := json.Marshal(&keychain)
    if err != nil {
        context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    cryptoConfig := config.LoadCryptoConfig()
    secret := generateRandomString(cryptoConfig.SecretLength)
    plaintext := string(keychainBytes)
    encryptedData, err := services.EncryptAES(plaintext, secret)
    if err != nil {
        context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    shieldedKeychain := domain.ShieldedKeychain{
        Code:   encryptedData,
        Secret: secret,
    }

    context.JSON(http.StatusOK, shieldedKeychain)
}

func DecryptKeychain(context *gin.Context) {
    var keychain domain.Keychain
    var shieldedKeychain domain.ShieldedKeychain
    if err := context.ShouldBind(&shieldedKeychain); err != nil {
        context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    plaintextBytes, err := services.DecryptAES(shieldedKeychain.Code, shieldedKeychain.Secret)
    if err != nil {
        context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    json.Unmarshal(plaintextBytes, &keychain)

    context.JSON(http.StatusOK, keychain)
}

func generateRandomString(length int) string {
    const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    seededRand := rand.New(rand.NewSource(time.Now().UnixNano()))

    bytes := make([]byte, length)
    for index := range bytes {
        bytes[index] = charset[seededRand.Intn(len(charset))]
    }
    return string(bytes)
}
