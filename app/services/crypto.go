package services

import (
    "crypto/aes"
    "crypto/cipher"
    "crypto/rand"
    "encoding/base64"
    "io"
)

func EncryptAES(plaintext string, secret string) (string, error) {
    block, err := aes.NewCipher([]byte(secret))
    if err != nil {
        return "", err
    }

    iv := make([]byte, aes.BlockSize)
    if _, err := io.ReadFull(rand.Reader, iv); err != nil {
        return "", err
    }

    plaintextBytes := []byte(plaintext)
    ciphertextBytes := make([]byte, len(plaintextBytes))
    cfb := cipher.NewCFBEncrypter(block, iv)
    cfb.XORKeyStream(ciphertextBytes, plaintextBytes)

    encryptedData := base64.StdEncoding.EncodeToString(append(iv, ciphertextBytes...))

    return encryptedData, nil
}

func DecryptAES(encryptedData string, secret string) ([]byte, error) {
    block, err := aes.NewCipher([]byte(secret))
    if err != nil {
        return nil, err
    }

    encryptedDataBytes, err := base64.StdEncoding.DecodeString(encryptedData)
    if err != nil {
        return nil, err
    }

    iv := encryptedDataBytes[:aes.BlockSize]
    ciphertextBytes := encryptedDataBytes[aes.BlockSize:]
    plaintextBytes := make([]byte, len(ciphertextBytes))
    cfb := cipher.NewCFBDecrypter(block, iv)
    cfb.XORKeyStream(plaintextBytes, ciphertextBytes)

    return plaintextBytes, nil
}
