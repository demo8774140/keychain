package routes

import (
    "gitlab.com/bitrewery/pass-shieldy/app/controllers"

    "github.com/gin-gonic/gin"
)

func Load(router *gin.Engine) {
    api := router.Group("/api")
    {
        v1 := api.Group("/v1")
        {
            v1.POST("/keychains/encryption", controllers.EncryptKeychain)
            v1.POST("/keychains/decryption", controllers.DecryptKeychain)
        }
    }
}