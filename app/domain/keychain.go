package domain

type Keychain struct {
    Keys []Key `json:"keys" binding:"required"`
}