package domain

type Key struct {
    Application string `json:"application" binding:"required"`
    Account     string `json:"account" binding:"required"`
    Password    string `json:"password" binding:"required"`
}