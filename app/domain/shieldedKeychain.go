package domain

type ShieldedKeychain struct {
    Code   string `form:"code" binding:"required"`
    Secret string `form:"secret" binding:"required"`
}